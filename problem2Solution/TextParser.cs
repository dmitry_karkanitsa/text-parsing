﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problem2Solution
{
    public class TextParser
    {
        public static Text ParseText(string fileName)
        {
            Text text = new Text();
            Sentence sentence = new Sentence();            
            Word word = new Word();
            bool isNewWord = true;
            bool isNewSentence = true;
            List<Sentence> sentences = new List<Sentence>();
            using (StreamReader sr = new StreamReader(fileName))
            {                
                while (!sr.EndOfStream)
                {
                    Symbol symbol = new Symbol((char)sr.Read());
                    if (symbol.Type == SymbolType.SignificantMark)
                    {
                        if (isNewSentence)
                        {
                            word.AddSymbol(new Symbol(symbol));
                            isNewSentence = false;
                            isNewWord = true;
                        }
                        else if (!isNewWord)
                        {
                            sentence.AddWord(new Word(word));
                            word.Clear();
                            word.AddSymbol(new Symbol(symbol));
                            isNewWord = true;
                        }
                        else if (isNewWord)
                        {
                            word.AddSymbol(new Symbol(symbol));
                        }
                    }
                    else if (symbol.Type == SymbolType.PunctuationMark)
                    {
                        isNewWord = false;
                        word.AddSymbolAfterWord(new Symbol(symbol));
                    }
                    else if (symbol.Type == SymbolType.SpaceMark)
                    {
                        isNewWord = false;
                    }
                    else if (symbol.Type == SymbolType.SentenceEndMark)
                    {
                        isNewWord = false;
                        word.AddSymbolAfterWord(new Symbol(symbol));
                        sentence.AddWord(new Word(word));
                        word.Clear();                        
                        sentence.DefineType();
                        text.AddSentence(new Sentence(sentence));
                        sentence.Clear();
                        isNewSentence = true;
                    }
                    else if (symbol.Type == SymbolType.LineEndMark)
                    {
                        if(isNewSentence)
                        {
                            text.GetSentence(text.CountSentences - 1).LastSentence = true;                            
                        }
                        else 
                        {
                            isNewWord = false;
                            word.EndOfLine = true;
                            sentence.AddWord(new Word(word));
                            word.Clear();
                        }                                                                                                                                               
                    }                    
                }
            }
            if(!isNewSentence)
            {
                text.AddSentence(sentence);
            }
            for(int i = 0; i < text.CountSentences; i++)
            {
                for(int j = 0; j < text.GetSentence(i).CountWords; j++)
                {
                    if(text.GetSentence(i).GetWord(j).GetString() == "")
                    {
                        text.GetSentence(i).RemoveWord(j);
                    }
                }
            }
/*            Text text = new Text(sentences);*/
            return text;
        }
    }
}
