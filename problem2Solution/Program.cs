﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problem2Solution
{
    class Program
    {
        static void Main(string[] args)
        {
            Text text = TextParser.ParseText("1,2input.txt");
            //1
            text.Sort();
            Console.Write(text);
            Console.WriteLine();
            //2
            var sentences = text.GetInterrogativeSentences();
            foreach (var s in sentences)
            {
                var words = s.GetWordsByLenght(6);
                foreach (var w in words)
                {
                    Console.WriteLine(w.GetString());
                }
            }
            Console.WriteLine();
            //3
            for (int i = 0; i < text.CountSentences; i++)
            {
                for (int j = 0; j < text.GetSentence(i).CountWords; j++)
                {
                    if (text.GetSentence(i).GetWord(j).GetSymbol(0).Consonant && text.GetSentence(i).GetWord(j).CountSymbols == 3)
                    {
                        text.GetSentence(i).RemoveWord(j);
                    }
                }
            }
            Console.Write(text);
            //4
            Sentence line = new Sentence("какая-то подстрока");
            for (int i = 0; i < text.GetSentence(0).CountWords; i++)
            {
                if (text.GetSentence(0).GetWord(i).CountSymbols == 3)
                {
                    text.GetSentence(0).RemoveWord(i);
                    i--;
                    for (int j = 0; j < line.CountWords; j++)
                    {
                        text.GetSentence(0).Insert(++i, line.GetWord(j));
                    }
                }
            }
            Console.Write(text.GetSentence(0));
            Console.WriteLine();
            Console.WriteLine();


            //cor
            Text text1 = TextParser.ParseText("1,2input.txt");
            Corcandance c = new Corcandance();
            string corcandance = c.GetString(text1);
            Console.Write(corcandance);
            Console.Read();
        }
    }
}
