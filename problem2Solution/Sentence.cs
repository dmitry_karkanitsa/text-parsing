﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problem2Solution
{
    public enum SentenceType
    {
        InterrogativeSentence,
        ExclamatorySentence,
        NarrativeSentence,
        MultiPointSentence,
        DirectSpeech,
        NoIdentify
    }
    public class Sentence : IComparable<Sentence>
    {
        private List<Word> words;
        private bool lastSentence;
        public bool LastSentence
        {
            get
            {
                return lastSentence;
            }
            set
            {
                lastSentence = value;                
            }
        }
        private SentenceType type;
        public SentenceType Type
        {
            get
            {
                return type;
            }
        }
        public int CountWords
        {
            get
            {
                return words.Count;
            }
        }
        public Sentence()
        {
            words = new List<Word>();
            type = SentenceType.NoIdentify;
        }        
        public Sentence(Sentence sentence)
        {
            words = new List<Word>();
            for(int i = 0; i < sentence.CountWords; i++)
            {
                Word word = new Word(sentence.GetWord(i));
                words.Add(word);
            }
            type = sentence.Type;
        }
        public Sentence(string str)
        {
            words = new List<Word>();
            type = SentenceType.NoIdentify;
            Word word = new Word();
            bool isNewWord = true;            
            for (int i = 0; i < str.Length; i++)
            {
                Symbol symbol = new Symbol(str[i]);
                if (symbol.Type == SymbolType.SignificantMark || symbol.Type == SymbolType.NoIdentifyMark)
                {                    
                    if (!isNewWord)
                    {
                        Word w = new Word(word);
                        words.Add(w);
                        word.Clear();                        
                        word.AddSymbol(new Symbol(symbol));
                        isNewWord = true;
                    }
                    else if (isNewWord)
                    {
                        word.AddSymbol(new Symbol(symbol));
                    }
                }
                else if (symbol.Type == SymbolType.PunctuationMark)
                {
                    isNewWord = false;
                    word.AddSymbolAfterWord(new Symbol(symbol));
                }
                else if (symbol.Type == SymbolType.SpaceMark)
                {
                    isNewWord = false;
                }
                else if (symbol.Type == SymbolType.SentenceEndMark)
                {
                    isNewWord = false;
                    word.AddSymbolAfterWord(new Symbol(symbol));
                    words.Add(new Word(word));
                    word.Clear();
                    type = DefineType(new Symbol(symbol));
                    break;                                        
                }
                else if (symbol.Type == SymbolType.LineEndMark)
                {
                    isNewWord = false;
                    word.AddSymbolAfterWord(new Symbol(symbol));
                    words.Add(new Word(word));
                    word.Clear();                                                                                                    
                }               
            }
            if (isNewWord)
            {
                words.Add(new Word(word));
            }
        }
        public Word GetWord(int i)
        {
            return words[i];
        }
        public void RemoveWord(int i)
        {
            if(i == words.Count-1)
            {
                words.RemoveAt(i);
                if(words[i-1].CountSymbolsAfterWord > 0)
                {
                    type = DefineType(words[i - 1].GetSymbolAfterWord(words[i - 1].CountSymbolsAfterWord - 1));
                }
                return;
            }
            words.RemoveAt(i);
            if(words.Count > 0)
            {
                if(words[0].CountSymbols > 0)
                {
                    Symbol symbol = new Symbol(char.ToUpper(words[0].GetSymbol(0).Smbl));
                    words[0].GetSymbol(0).SetSymbol(symbol);
                }
            }
        }
        public void AddWord(Word word)
        {
            if (word.CountSymbolsAfterWord > 0)
            {
                type = DefineType(word.GetSymbolAfterWord(word.CountSymbolsAfterWord - 1));
            }
            else type = SentenceType.NoIdentify;
            words.Add(new Word(word));
        }
        public static SentenceType DefineType(Symbol sm)
        {
            if (sm.Smbl == '.')
            {
                return SentenceType.NarrativeSentence;
            }
            else if (sm.Smbl == '!')
            {
                return SentenceType.ExclamatorySentence;
            }
            else if (sm.Smbl == '?')
            {
                return SentenceType.InterrogativeSentence;
            }
            else return SentenceType.NoIdentify;
        }
        public void DefineType()
        {
            if(words.Count > 0)
            {
                if(words[words.Count - 1].CountSymbolsAfterWord > 0)
                {
                    type = DefineType(words[words.Count - 1].GetSymbolAfterWord(words[words.Count - 1].CountSymbolsAfterWord - 1));
                }
            }
        }
        public void Insert(int i, Word word)
        {
            if(i == 0)
            {
                Word w = new Word(word);
                if(w.CountSymbols > 0)
                {
                    w.GetSymbol(0).SetSymbol(char.ToUpper(w.GetSymbol(0).Smbl));
                    if(words.Count > 0 )
                    {
                        if(words[0].CountSymbols > 0)
                        {
                            words[0].GetSymbol(0).SetSymbol(char.ToLower(words[0].GetSymbol(0).Smbl));
                        }
                    }
                }
                words.Insert(i, w);
            }
            else if (i == words.Count - 1)
            {
                if (word.CountSymbolsAfterWord > 0)
                {
                    Word w = new Word(word);
                    type = DefineType(w.GetSymbolAfterWord(w.CountSymbolsAfterWord - 1));
                }
                else type = SentenceType.NoIdentify;
            }            
            else
            {
                Word w = new Word(word);
                words.Insert(i, w);
            }
        }
        public void Clear()
        {
            words.Clear();
            type = SentenceType.NoIdentify;
        }
        int IComparable<Sentence>.CompareTo(Sentence other)
        {
            if (words.Count > other.CountWords)
            {
                return 1;
            }
            else if (words.Count < other.CountWords)
            {
                return -1;
            }
            return 0;
        }
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            for(int i = 0; i < words.Count; i++)
            {
                s.Append(words[i].ToString()+" ");
            }
            return s.ToString();
        }
        public List<Word> GetWordsByLenght(int length)
        {
            SortedSet<Word> words = new SortedSet<Word>();
            foreach(var word in this.words)
            {
                if(word.CountSymbols == length)
                {
                    words.Add(new Word(word));
                }
            }
            return words.ToList() ;
        }
    }
}
