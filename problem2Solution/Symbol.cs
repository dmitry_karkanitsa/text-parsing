﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problem2Solution
{
    public enum SymbolType
    {
        PunctuationMark,
        SentenceEndMark,
        LineEndMark,
        SignificantMark,
        SpaceMark,
        NoIdentifyMark
    }
    public class Symbol
    {
        private char[] sentenceEndMarks = { '.', '!', '?' };
        private char[] punctuationMarks = {  ',', ';', '(' , ')', ':', '—', '/', '|'};
        private char[] endOfLineMarks = { '\n' };
        private char[] spaceMarks = { ' ', '\t', '\r' };
        private char[] vowelMarks = { 'a', 'A', 'y', 'Y', 'u', 'U', 'i', 'I', 'o', 'O', 'а', 'А', 'е', 'Е', 'ё', 'Ё', 'и', 'И', 'о', 'О', 'у', 'У', 'э', 'Э', 'ы', 'Ы', 'ю', 'Ю', 'я', 'Я' };
        private SymbolType type;
        public SymbolType Type
        {
            get
            {
                return type;
            }
        }
        private bool vowel;
        private bool consonant;
        private char symbol;
        public char Smbl
        {
            get
            {
                return symbol;
            }
        }
        public bool Vowel
        {
            get
            {
                return vowel;
            }
        }
        public bool Consonant
        {
            get
            {
                return consonant;
            }
        }        
        public Symbol(Symbol symbol)
        {
            this.symbol = symbol.Smbl;
            type = symbol.type;
            vowel = symbol.Vowel;
            consonant = symbol.Consonant;
        }
        public Symbol(char symbol) {
            consonant = false;
            vowel = false;
            if (char.IsLetter(symbol))
            {
                if(vowelMarks.Contains(symbol))
                {
                    vowel = true;
                    consonant = false;
                }
                else
                {
                    consonant = true;
                    vowel = false;
                }
                type = SymbolType.SignificantMark;
            }           
            else if (spaceMarks.Contains(symbol))
                type = SymbolType.SpaceMark;
            else if (endOfLineMarks.Contains(symbol))
                type = SymbolType.LineEndMark;
            else if (punctuationMarks.Contains(symbol))
                type = SymbolType.PunctuationMark;
            else if (sentenceEndMarks.Contains(symbol))
                type = SymbolType.SentenceEndMark;
            else
                type = SymbolType.NoIdentifyMark;
            this.symbol = symbol;
        }
        public void SetSymbol(char symbol)
        {
            consonant = false;
            vowel = false;
            if (char.IsLetter(symbol))
            {
                if (vowelMarks.Contains(symbol))
                {
                    vowel = true;
                    consonant = false;
                }
                else
                {
                    consonant = true;
                    vowel = false;
                }
                type = SymbolType.SignificantMark;
            }
            else if (spaceMarks.Contains(symbol))
                type = SymbolType.SpaceMark;
            else if (endOfLineMarks.Contains(symbol))
                type = SymbolType.LineEndMark;
            else if (punctuationMarks.Contains(symbol))
                type = SymbolType.PunctuationMark;
            else if (sentenceEndMarks.Contains(symbol))
                type = SymbolType.SentenceEndMark;
            else
                type = SymbolType.NoIdentifyMark;
            this.symbol = symbol;
        }
        public void SetSymbol(Symbol symbol)
        {
            type = symbol.Type;
            vowel = symbol.Vowel;
            consonant = symbol.Consonant;
            this.symbol = symbol.Smbl;
        }
        public override string ToString()
        {
            return symbol.ToString();
        }
    }
}
