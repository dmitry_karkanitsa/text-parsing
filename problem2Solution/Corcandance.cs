﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problem2Solution
{    
    public class Corcandance
    {
        private Dictionary<string, List<int>> corcandance ;
        private int lineCounter;
        private int CompareTo(Word w1, Word w2)
        {
            return String.Compare(w1.GetString().ToLower(), w2.GetString().ToLower());
        }
        /*public string GetCorcandance()
        {
            StringBuilder sr = new StringBuilder();
            foreach(var c in corcandance)
            {

            }
        }*/
        public string GetString(Text text)
        {
            corcandance = new Dictionary<string, List<int>>();
            lineCounter = 1;
            for(int i = 0; i < text.CountSentences; i++)
            {
                for(int j = 0; j < text.GetSentence(i).CountWords; j++)
                {
                    if(!corcandance.ContainsKey(text.GetSentence(i).GetWord(j).GetString().ToLower()))
                    {
                        corcandance.Add(text.GetSentence(i).GetWord(j).GetString().ToLower(), new List<int> { lineCounter });
                    }
                    else
                    {
                        corcandance[text.GetSentence(i).GetWord(j).GetString().ToLower()].Add(lineCounter);
                    }
                    if (text.GetSentence(i).GetWord(j).EndOfLine)
                    {
                        lineCounter++;
                    }
                }
                if(text.GetSentence(i).LastSentence)
                {
                    lineCounter++;
                }
            }
            corcandance = corcandance.OrderBy(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value);
            string cor = "";            
            foreach(var p in corcandance)
            {
                cor += p.Key;
                cor += "...";
                cor += p.Value.Count.ToString();
                cor += ":";
                cor += p.Value[0].ToString();
                cor += " ";
                for(int i = 1; i < p.Value.Count; i++)
                {
                    if(p.Value[i] != p.Value[i-1])
                    {
                        cor += p.Value[i].ToString();
                        cor += " ";
                    }
                }
                cor += "\n";
            }
            return cor;
        }
    }
}
