﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problem2Solution
{
    public class Word : IComparable<Word>
    {
        private List<Symbol> symbols;
        private List<Symbol> symbolsAfterWord;
        private bool endOfLine;
        public bool EndOfLine
        {
            get
            {
                if (endOfLine)
                    return true;
                return false;
            }
            set
            {
                endOfLine = true;
            }
        }
        public int CountSymbols
        {
            get
            {
                return symbols.Count;
            }
        }
        public int CountSymbolsAfterWord
        {
            get
            {
                return symbolsAfterWord.Count;
            }
        }
        public Word(string word)
        {
            endOfLine = false;
            for(int i = 0; i < word.Length; i++)
            {
                Symbol symbol = new Symbol(word[i]);
                if (symbol.Type == SymbolType.SpaceMark)
                    continue;
                else if (symbol.Type == SymbolType.SignificantMark || symbol.Type == SymbolType.NoIdentifyMark)
                    symbols.Add(symbol);
                else if (symbol.Type == SymbolType.LineEndMark)
                {
                    symbolsAfterWord.Add(new Symbol(symbol));
                    endOfLine = true;
                    break;
                }
                else if (symbol.Type == SymbolType.PunctuationMark || symbol.Type == SymbolType.SentenceEndMark)
                    symbolsAfterWord.Add(symbol);                    
            }
        }
        public Word()
        {
            symbols = new List<Symbol>();
            symbolsAfterWord = new List<Symbol>();
            endOfLine = false;
        }
        public Word(Word word)
        {
            symbols = new List<Symbol>();
            symbolsAfterWord = new List<Symbol>();
            for (int i = 0; i < word.CountSymbols; i++)
                symbols.Add(new Symbol(word.GetSymbol(i)));
            for (int i = 0; i < word.CountSymbolsAfterWord; i++)
                symbolsAfterWord.Add(new Symbol(word.GetSymbolAfterWord(i)));
            endOfLine = word.EndOfLine;
        }
        public void AddSymbol(Symbol symbol)
        {
            if (symbol.Type == SymbolType.NoIdentifyMark || symbol.Type == SymbolType.SignificantMark)
                symbols.Add(new Symbol(symbol));
        }
        public void AddSymbolAfterWord(Symbol symbol){
            if (endOfLine || symbol.Type == SymbolType.NoIdentifyMark || symbol.Type == SymbolType.SignificantMark || symbol.Type == SymbolType.SpaceMark)
                return;
            if(symbol.Type == SymbolType.LineEndMark)
            {
                symbolsAfterWord.Add(new Symbol(symbol));
                endOfLine = true;
            }
            else if(symbol.Type == SymbolType.SentenceEndMark)
            {
                symbolsAfterWord.Add(new Symbol(symbol));
            }
            else if(symbol.Type == SymbolType.PunctuationMark)
            {
                for(int i = 0; i < symbolsAfterWord.Count; i++)
                {
                    if (symbolsAfterWord[i].Type == SymbolType.SentenceEndMark)
                        return;
                }
                symbolsAfterWord.Add(new Symbol(symbol));
            }

        }
        public void DeleteSymbol(int i)
        {
            symbols.RemoveAt(i);
        }
        public void DeleteSymbolAfterWord(int i)
        {
            if(symbolsAfterWord[i].Type == SymbolType.LineEndMark)
                endOfLine = false;
            symbolsAfterWord.RemoveAt(i);
        }
        public void InsertSymbol(int i, Symbol symbol)
        {
            if (symbol.Type == SymbolType.NoIdentifyMark || symbol.Type == SymbolType.SignificantMark)
                symbols.Insert(i, new Symbol(symbol));
        }
        public Symbol GetSymbol(int i)
        {
            return symbols[i];         
        }
        public Symbol GetSymbolAfterWord(int i)
        {
            return symbolsAfterWord[i];        
        }
        public Symbol this[int i]
        {
            get
            {
                Symbol s = new Symbol(symbols[i]);
                return s;
            }
        }
        public void Clear()
        {
            symbols.Clear();
            symbolsAfterWord.Clear();
            endOfLine = false;
        }
        public string GetString()
        {
            StringBuilder s = new StringBuilder();
            for(int i = 0; i < symbols.Count; i++)
            {
                s.Append(symbols[i].Smbl);
            }
            return s.ToString() ;
        }
        int IComparable<Word>.CompareTo(Word other)
        {
            return String.Compare(GetString().ToLower(), other.GetString().ToLower());
        }
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            for(int i = 0; i < symbols.Count; i++)
            {
                s.Append(symbols[i]);
            }
            for(int i = 0; i < symbolsAfterWord.Count; i++)
            {
                s.Append(symbolsAfterWord[i]);
            }
            return s.ToString();
        }

    }
}
