﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problem2Solution
{
    public class Text
    {
        private List<Sentence> sentences;
        public int CountSentences
        {
            get
            {
                return sentences.Count;
            }
        }
        public Text()
        {
            sentences = new List<Sentence>();
        }
        public void AddSentence(Sentence sentence)
        {
            sentences.Add(new Sentence(sentence));
        }
        public void RemoveSentence(int i)
        {
            sentences.RemoveAt(i);
        }
        public Text(List<Sentence> sentences)
        {
            List<Sentence> sentences1 = new List<Sentence>();
            for(int i = 0; i < sentences.Count; i++)
            {
                sentences1.Add(new Sentence(sentences[i]));
            }
    }
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            foreach(var i in sentences)
            {
                s.Append(i.ToString());
            }
            return s.ToString();          
        }
        public void Sort()
        {
            sentences.Sort();
        }
        public List<Sentence> GetInterrogativeSentences()
        {
            List<Sentence> sentences = new List<Sentence>();
            foreach (var sn in this.sentences)
            {
                if (sn.Type == SentenceType.InterrogativeSentence)
                {
                    sentences.Add(sn);
                }
            }
            return sentences;
        }
        public Sentence GetSentence(int i)
        {
            return sentences[i];
        }
    }
}
